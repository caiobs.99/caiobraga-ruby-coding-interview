class Tweet < ApplicationRecord
  scope :posted_today, ->{ where("created_at > ?", 1.day.ago) }
  scope :unique_by_user, ->{ where("id IN (SELECT MAX(id) FROM tweets GROUP BY user_id)") }

  belongs_to :user

  validates :body, length: {
    maximum: 180
  }
  validate :ensure_unique_body

  private

  def ensure_unique_body
    user_tweets = Tweet.where(user: user).posted_today

    user_tweets.each do |tweet|
      next if tweet.body != body

      raise ActiveRecord::RecordInvalid("tweet text must be unique!")
    end
  end
end
