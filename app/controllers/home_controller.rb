class HomeController < ApplicationController
  def index
    @tweets = Tweet.includes(:user, user: :company)
      .distinct(:user_id)
      .unique_by_user
      .order("created_at DESC")
      .first(20)
      .to_a
  end
end
