require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe "validations" do
    it { is_expected.to validate_length_of(:body).is_at_most(180) }

    context "when user is the same" do
      let(:user) { create(:user) }

      before { create(:tweet, user: user, body: "some text", created_at: Time.now) }

      context "when multiple tweets are made" do
        context "and every tweet is unique" do
          it "it allows for the creation of the tweet" do
            expect { described_class.create(user: user, body: "some other text", created_at: Time.now) }
              .to_not raise_error
          end
        end

        context "and there are repeated tweets" do
          it "it allows for the creation of the tweet" do
            expect { described_class.create(user: user, body: "some text", created_at: Time.now) }
              .to raise_error
          end
        end
      end
    end

    context "when user is different" do
      context "when multiple tweets are made" do
        let(:user) { create(:user) }

        before { create(:tweet, body: "some text", created_at: Time.now) }

        context "and every tweet is unique" do
          context "and every tweet is unique" do
            it "it allows for the creation of the tweet" do
              expect { described_class.create(user: user, body: "some other text", created_at: Time.now) }
                .to_not raise_error
            end
          end
        end

        context "and there are repeated tweets" do
          context "and there are repeated tweets" do
            it "it allows for the creation of the tweet" do
              expect { described_class.create(user: user, body: "some text", created_at: Time.now) }
                .to_not raise_error
            end
          end
        end
      end
    end
  end
end
